#version 330 core

in vec3 Col;

out vec4 FragColor;

uniform float first;
uniform sampler2DMS depthText;
uniform float near;
uniform float far;

float epsilon = 0.0001; // To combat z-fighting

float linearizeDepth(float depth) {
    float z = depth * 2.0 - 1.0; // back to NDC 
    return (2.0 * near * far) / (far + near - z * (far - near));	
}

void main() {
	if (first != 1.0f) {
		ivec2 texCoords = ivec2(gl_FragCoord.x, gl_FragCoord.y);
		float depth = texelFetch(depthText, texCoords, 0).s;
		float curDepth = gl_FragCoord.z;
		if (curDepth <= depth + epsilon)
			discard;
	}
	FragColor = vec4(Col, 0.5f);
}