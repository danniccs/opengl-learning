#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <filesystem>
#include <vector>

// Include glm for matrix math
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>

// Include stb for image loading
#include <stb-master/stb_image.h>

#include "Camera.h" // Camera class
#include "Shader.h" // Shader class
#include "Light.h" // Light class
#include "Model.h" // Model class
#include "misc_sources.h" // framebuffer size callback and input processing
#include "texture_loader.h" // Utility function for loading textures (generates texture)

namespace fs = std::filesystem;

// Set number of point lights and instances
const unsigned int NR_POINT_LIGHTS = 4;
const unsigned int NR_SPOT_LIGHTS = 1;
const unsigned int NR_INSTANCES = 1;

const glm::vec3 sunYellow(0.9765f, 0.8431f, 0.1098f);

// Set the initial camera positions
Camera cam(glm::vec3(0.0f, 0.0f, 3.0f), glm::vec3(0.0f, 1.0f, 0.0f));

// Current mouse positions
double lastX(400.0), lastY(300.0);

// Set initial values for times
float deltaTime(0.0f);
float lastFrame(0.0f);
float currentFrame(0.0f);

// Declare callbacks and input processing function
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void processInput(GLFWwindow* window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

namespace toggles {  // Only changed by input processing
    bool bKeyPressed = false;
    bool nKeyPressed = false;
    bool g_showNorms{ false };
    bool g_blinn{ true };
};

int main() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // 4 samples for multisampling
    glfwWindowHint(GLFW_SAMPLES, 4);
    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    #endif

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (!window) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // GLAD loader to be able to use OpenGL functions (gets function pointers from address)
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // Check if negative swap interval values are supported, and activate v-sync
    bool supported = static_cast<bool>(glfwExtensionSupported("WGL_EXT_swap_control_tear")) ||
        static_cast<bool>(glfwExtensionSupported("GLX_EXT_swap_control_tear"));
    if (supported)
        glfwSwapInterval(-1);
    else
        glfwSwapInterval(1);

    unsigned int err;
    while ((err = glGetError()) != GL_NO_ERROR) {
        std::cout << "GLAD error: " << std::hex << err << '\n';
    }
    
    // Set the callback functions
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // Capture mouse and listen to it
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwSetCursorPos(window, lastX, lastY);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetKeyCallback(window, key_callback);

    // Define the clear color
    glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
    // Draw in Wireframe mode
    //glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    // Draw in normal mode
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    // Enable the Z-buffer (Depth buffer) and stencil test
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
    // Enable multisampling
    glEnable(GL_MULTISAMPLE);

    // compile and link the shader programs
    Shader sProg("lighting_vert_shader.glsl", "lighting_frag_shader.glsl");
    Shader lProg("light_vert_shader.glsl", "light_frag_shader.glsl");
    Shader simpleShaders("lighting_vert_shader.glsl", "simple_frag_shader.glsl");
    Shader normalVisProg("normal_vert_shader.glsl", "normal_frag_shader.glsl", "normal_geom_shader.glsl");
    Shader floorProg("floor_vert.glsl", "floor_frag.glsl");
    Shader simpleNorm("simple_normal_vert.glsl", "normal_frag_shader.glsl", "normal_geom_shader.glsl");

    // Get the uniform IDs in the vertex shader
    const int sViewID = sProg.getUnif("view");
    const int sProjID = sProg.getUnif("projection");
    const int simViewID = simpleShaders.getUnif("view");
    const int simProjID = simpleShaders.getUnif("projection");
    const int lViewID = lProg.getUnif("view");
    const int lProjID = lProg.getUnif("projection");
    const int normViewID = normalVisProg.getUnif("view");
    const int normProjID = normalVisProg.getUnif("projection");
    const int floorViewID = floorProg.getUnif("view");
    const int floorProjID = floorProg.getUnif("projection");
    const int simpNormViewID = simpleNorm.getUnif("view");
    const int simpNormProjID = simpleNorm.getUnif("projection");

    // Floor VAO, texture and model
    unsigned int VAO, VBO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sources::quadVertices), sources::quadVertices, GL_STATIC_DRAW);    
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
    glEnableVertexAttribArray(2);
    glBindVertexArray(0);
    unsigned int floorTex = loadTexture("wood.png", true);
    glm::mat4 floorModel = glm::mat4(1.0f);
    floorModel = glm::translate(floorModel, glm::vec3(0.0f, -0.5f, 0.0f));
    floorModel = glm::rotate(floorModel, -glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    floorModel = glm::scale(floorModel, glm::vec3(20.0f, 20.0f, 1.0f));
    floorProg.use();
    floorProg.setUnifS("model", floorModel);
    floorProg.setUnifS("floorTexture", 0);
    simpleNorm.use();
    simpleNorm.setUnifS("model", floorModel);

    // Load the models
    Model nanosuit("nanosuit/nanosuit.obj", true);
    nanosuit.getTextureLocations(sProg);
    glm::mat3 normMats[NR_INSTANCES];
    glm::mat4 modelMats[NR_INSTANCES];

    Model bulb("ESLamp_obj/ESLamp.obj");
    bulb.getTextureLocations(lProg);
    glm::mat4 bulbMods[NR_POINT_LIGHTS + NR_SPOT_LIGHTS];

    // Declare the model, view and projection matrices
    glm::mat4 view;
    glm::mat4 projection;

    // Set the directional light attributes
    Light dirLight("directionalLight", sProg, true);

    // Set point light attributes
    Light pointLights[NR_POINT_LIGHTS];
    for (unsigned int i = 0; i < NR_POINT_LIGHTS; i++) {
        std::string unifName = "pointLights[";
        unifName.append(std::to_string(i)).append("]");
        pointLights[i].initVals(unifName, sProg, false,
            sources::pointLightAttenuationValues[i][0], sources::pointLightAttenuationValues[i][1],
            sources::pointLightAttenuationValues[i][2]);
    }

    // Set flashlight attributes
    float cutOff = glm::cos(glm::radians(10.0f));
    float outerCutOff = glm::cos(glm::radians(20.0f));
    Light flashLight("flashLight", sProg, false, 1.0f, 0.027f, 0.0028f, cutOff, outerCutOff);

    // Set spotlight attributes
    cutOff = glm::cos(glm::radians(30.5f));
    outerCutOff = glm::cos(glm::radians(50.0f));
    Light spotLight("spotLight", sProg, false, 1.0f, 0.14f, 0.07f, cutOff, outerCutOff);

    while (!glfwWindowShouldClose(window)) {

        currentFrame = static_cast <float> (glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        processInput(window);

        // render
        // Clear the color
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        // Enable gamma correction. This should only be enabled before drawing to the display framebuffer
        glEnable(GL_FRAMEBUFFER_SRGB);
        /*
        Update Matrices
        ---------------
        */
        // Update the camera view
        view = cam.GetViewMatrix();
        // Create a matrix to maintain directions in view space
        glm::mat3 dirNormMat(glm::transpose(glm::inverse(view)));
        // Update the projection matrix
        projection = glm::perspective(glm::radians(cam.Zoom), 800.0f / 600.0f, 0.1f, 100.0f);

        // Set the positions of the point lights
        glm::vec3 pointLightColors[NR_POINT_LIGHTS];
        for (unsigned int i = 0; i < NR_POINT_LIGHTS; i++)
            pointLightColors[i] = glm::vec3(1.0f);

        // Set the position and direction of the spotlight
        glm::vec3 spotLightPos(0.0f, 3.8f, 0.0f);
        glm::vec3 spotLightDir(0.0f, -1.0f, 0.0f);
        // Set the colors of the spot light
        glm::vec3 spotLightColor(1.0f, 1.0f, 0.0f);

        // Render the floor
        floorProg.use();
        floorProg.setUnif(floorViewID, view);
        floorProg.setUnif(floorProjID, projection);
        floorProg.setUnifS("normMat", glm::mat3(glm::transpose(glm::inverse(floorModel))));
        floorProg.setUnifS("lightPos", glm::vec3(0.0f, 0.0f, 0.0f));
        floorProg.setUnifS("viewPos", cam.Position);
        floorProg.setUnifS("blinn", toggles::g_blinn);
        glBindVertexArray(VAO);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, floorTex);
        glDrawArrays(GL_TRIANGLES, 0, 6);

        // use the program and set all uniform values before draw call
        sProg.use();
        sProg.setUnif(sViewID, view);
        sProg.setUnif(sProjID, projection);
        // Set directional light coordinates and color
        dirLight.setDir(glm::vec3(-0.4f, -1.0f, 0.0f), dirNormMat);
        dirLight.setColors(glm::vec3(1.0f, 1.0f, 0.0f), 0.2f, 0.5f, 0.8f);
        // Set point light attributes
        for (unsigned int i = 0; i < NR_POINT_LIGHTS; i++) {
            pointLights[i].setPos(sources::pointLightPositions[i], view);
            pointLights[i].setColors(pointLightColors[i], 0.2f, 0.5f, 0.8f);
        }
        // Set flashlight colors and position/direction
        flashLight.setPos(cam.Position, view);
        flashLight.setDir(cam.Front, dirNormMat);
        flashLight.setColors(glm::vec3(1.0f), 0.0f, 0.8f, 0.85f);
        // Set spotlight colors and position/direction
        spotLight.setPos(spotLightPos, view);
        spotLight.setDir(spotLightDir, dirNormMat);
        spotLight.setColors(spotLightColor, 0.2f, 0.45f, 0.7f);
        // Update the model and normal matrices for nanosuits
        for (unsigned int i = 0; i < NR_INSTANCES; i++) {
            modelMats[i] = glm::mat4(1.0f);
            //modelMats[i] = glm::translate(modelMats[i], glm::vec3(2.0f * static_cast<float>(i) + 1.0f, 0.0f, i));
            modelMats[i] = glm::scale(modelMats[i], glm::vec3(0.2f));
            normMats[i] = glm::mat3(glm::transpose(glm::inverse(view * modelMats[i])));
        }

        // Call the model draw function
        nanosuit.Draw(sProg, NR_INSTANCES, modelMats, normMats);

        // Draw the normal vectors
        if (toggles::g_showNorms) {
            normalVisProg.use();
            normalVisProg.setUnif(normViewID, view);
            normalVisProg.setUnif(normProjID, projection);
            nanosuit.Draw(normalVisProg, NR_INSTANCES, modelMats, normMats);

            simpleNorm.use();
            simpleNorm.setUnif(simpNormViewID, view);
            simpleNorm.setUnif(simpNormProjID, projection);
            simpleNorm.setUnifS("normMat", glm::mat3(glm::transpose(glm::inverse(view * floorModel))));
            glBindVertexArray(VAO);
            glDrawArrays(GL_TRIANGLES, 0, 6);
        }

        // Now do the same for the light sources
        lProg.use();
        // we use the same view, projection and normal matrices
        lProg.setUnif(lViewID, view);
        lProg.setUnif(lProjID, projection);
        // Update model matrices
        for (unsigned int i = 0; i < NR_POINT_LIGHTS; i++) {
            bulbMods[i] = glm::mat4(1.0f);
            bulbMods[i] = glm::translate(bulbMods[i], sources::pointLightPositions[i]);
            bulbMods[i] = glm::rotate(bulbMods[i], glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
            bulbMods[i] = glm::scale(bulbMods[i], glm::vec3(0.05f));
        }
        bulbMods[NR_POINT_LIGHTS] = glm::mat4(1.0f);
        bulbMods[NR_POINT_LIGHTS] = glm::translate(bulbMods[NR_POINT_LIGHTS], spotLightPos);
        bulbMods[NR_POINT_LIGHTS] = glm::rotate(bulbMods[NR_POINT_LIGHTS], glm::radians(180.0f), glm::vec3(0.0f, 0.0f, 1.0f));
        bulbMods[NR_POINT_LIGHTS] = glm::scale(bulbMods[NR_POINT_LIGHTS], glm::vec3(0.05f));
        bulb.Draw(lProg, NR_POINT_LIGHTS + NR_SPOT_LIGHTS, bulbMods, NULL);

        // buffer swap and event poll
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();
    glDeleteBuffers(1, &VBO);
    glDeleteVertexArrays(1, &VAO);
    glDeleteTextures(1, &floorTex);
    return 0;
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos) {
    double xoffset = xpos - lastX;
    double yoffset = lastY - ypos; // reversed since y-coordinates range from bottom to top
    lastX = (xpos);
    lastY = (ypos);

    cam.ProcessMouseMovement(static_cast <float> (xoffset), static_cast <float> (yoffset), true);
}

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    cam.ProcessMouseScroll(static_cast <float> (yoffset));
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_PRESS) {
        cam.MovementSpeed /= 2.0f;
    }
    if (key == GLFW_KEY_LEFT_SHIFT && action == GLFW_RELEASE) {
        cam.MovementSpeed *= 2.0f;
    }
}

void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::RIGHT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::UP, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS)
        cam.ProcessKeyboard(Camera_Movement::DOWN, deltaTime);

    if (glfwGetKey(window, GLFW_KEY_N) == GLFW_PRESS && !toggles::nKeyPressed) {
        toggles::g_showNorms = !toggles::g_showNorms;
        toggles::nKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_N) == GLFW_RELEASE) {
        toggles::nKeyPressed = false;
    }

    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS && !toggles::bKeyPressed) {
        toggles::g_blinn = !toggles::g_blinn;
        toggles::bKeyPressed = true;
    }
    if (glfwGetKey(window, GLFW_KEY_B) == GLFW_RELEASE) {
        toggles::bKeyPressed = false;
    }
}