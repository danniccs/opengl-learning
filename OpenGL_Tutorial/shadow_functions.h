#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 calculateOLightFrustum(const glm::mat4& view_projection_mat,
								 const glm::mat4& lightViewMat, float mapSize);

glm::mat4 altOLightFrustum(const glm::mat4& view_projection_mat, const glm::vec3& lightDir,
						   const glm::vec3& up, const glm::vec3& right, float mapSize);