#version 330 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2DMS screenTexture0;
uniform sampler2DMS screenTexture1;
uniform sampler2DMS screenTexture2;
uniform sampler2DMS screenTexture3;

vec3 over(vec4 src, vec4 dst);

vec4 under(vec4 src, vec4 dst);

void main() {
    ivec2 texSize = textureSize(screenTexture0);
    ivec2 newTexCoords = ivec2(TexCoords * texSize);
    vec4 srcCol;
    FragColor = vec4(0);

    for (int i = 0; i < 4; ++i)
        srcCol += texelFetch(screenTexture0, newTexCoords, i);
    srcCol /= 4;

    FragColor = srcCol;

    srcCol = vec4(0);
    for (int i = 0; i < 4; ++i)
        srcCol += texelFetch(screenTexture1, newTexCoords, i);
    srcCol /= 4;

    FragColor = under(srcCol, FragColor);
    
    srcCol = vec4(0);
    for (int i = 0; i < 4; ++i)
        srcCol += texelFetch(screenTexture2, newTexCoords, i);
    srcCol /= 4;
    
    FragColor = under(srcCol, FragColor);
    
    srcCol = vec4(0);
    for (int i = 0; i < 4; ++i)
        srcCol += texelFetch(screenTexture3, newTexCoords, i);
    srcCol /= 4;
    
    FragColor = under(srcCol, FragColor);
}

vec3 over(vec4 src, vec4 dst) {
    return src.rgb * src.a + dst.rgb * (1 - src.a);
}

vec4 under(vec4 src, vec4 dst) {
    vec4 new_dst;
    new_dst.rgb = dst.rgb * dst.a + src.rgb * src.a * (1 - dst.a);
    new_dst.a = src.a - src.a * dst.a + dst.a;
    return new_dst;
}