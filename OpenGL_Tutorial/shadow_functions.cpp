#include "shadow_functions.h"
#include <vector>
#include <algorithm>
#include <iostream>
#include <limits>

glm::mat4 calculateOLightFrustum(const glm::mat4& view_projection_mat,
								 const glm::mat4& lightViewMat, float mapSize)
{
	// Calculate matrix inverses
	glm::mat4 inverse_mat = lightViewMat * glm::inverse(view_projection_mat);
	// Calculate local light-space view frustum corners
	std::vector<glm::vec4> corners(8);
	for (int i = -1; i <= 1; i += 2) {
		for (int j = -1; j <= 1; j += 2) {
			for (int k = -1; k <= 1; k += 2) {
				glm::vec4 inverted = inverse_mat * glm::vec4(i, j, k, 1.0f);
				corners.push_back(inverted / inverted.w);
			}
		}
	}

	// Calculate the bounding box of the corners
	float xmin = corners[0].x, xmax = corners[0].x;
	float ymin = corners[0].y, ymax = corners[0].y;
	float zmax = 0.0f;

	for (auto elem : corners) {
		xmin = std::min(xmin, elem.x);
		ymin = std::min(ymin, elem.y);

		xmax = std::max(xmax, elem.x);
		ymax = std::max(ymax, elem.y);
		zmax = std::max(zmax, std::abs(elem.z));
	}

	// Calculate the orthographic light projection matrix
	float extent2 = std::max(xmax - xmin, ymax - ymin);
	float quantStep = extent2 / mapSize;
	xmin = std::floor(xmin / quantStep) * quantStep;
	ymin = std::floor(ymin / quantStep) * quantStep;
	xmax = std::floor(xmax / quantStep) * quantStep;
	ymax = std::floor(ymax / quantStep) * quantStep;
	zmax = std::floor(zmax / quantStep) * quantStep;
	return glm::ortho(xmin, xmax, ymin, ymax, 0.1f, zmax);
}

glm::mat4 altOLightFrustum(const glm::mat4& view_projection_mat, const glm::vec3& lightDir,
						   const glm::vec3& up, const glm::vec3& right, float mapSize)
{
	glm::vec3 normLightDir = glm::normalize(lightDir);
	glm::vec3 normRight = glm::normalize(right);
	glm::vec3 normUp = glm::normalize(up);
	glm::mat4 invMat = glm::inverse(view_projection_mat);

	// Calculate local light-space view frustum corners
	constexpr float minFloat = std::numeric_limits<float>::min();
	constexpr float maxFloat = std::numeric_limits<float>::max();
	float zmax = minFloat;
	float xmin = maxFloat;
	float xmax = minFloat;
	float ymin = maxFloat;
	float ymax = minFloat;
	for (int i = -1; i <= 1; i += 2) {
		for (int j = -1; j <= 1; j += 2) {
			for (int k = -1; k <= 1; k += 2) {
				glm::vec4 hWC = invMat * glm::vec4(i, j, k, 1.0f);
				glm::vec3 worldCorner = glm::vec3(glm::vec3(hWC.x, hWC.y, hWC.z) / hWC.w);
				zmax = std::max(std::abs(glm::dot(normLightDir, worldCorner)), zmax);
				xmin = std::min(glm::dot(normRight, worldCorner), xmin);
				ymin = std::min(glm::dot(normUp, worldCorner), ymin);
				xmax = std::max(glm::dot(normRight, worldCorner), xmax);
				ymax = std::max(glm::dot(normUp, worldCorner), ymax);
			}
		}
	}

	// Calculate the orthographic light projection matrix
	// Discretize it so it only increases in steps corresponding to shadow map size
	// to avoid artifacts while moving
	float extent2 = std::max(xmax - xmin, ymax - ymin);
	float quantStep = extent2 / mapSize;
	xmin = std::floor(xmin / quantStep) * quantStep;
	ymin = std::floor(ymin / quantStep) * quantStep;
	xmax = std::floor(xmax / quantStep) * quantStep;
	ymax = std::floor(ymax / quantStep) * quantStep;
	zmax = std::floor(zmax / quantStep) * quantStep;

	return glm::ortho(xmin, xmax, ymin, ymax, 0.1f, zmax);
	//return glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, 0.1f, 30.0f);
}