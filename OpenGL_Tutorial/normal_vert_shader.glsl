#version 430 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNorm;
layout(location = 2) in vec2 atexCoords;
layout(location = 4) in mat4 aModel;
layout(location = 8) in mat3 aNormMat;

out VS_OUT {
	vec3 normal;
} vs_out;

uniform mat4 view;

void main() {
	gl_Position = view * aModel * vec4(aPos, 1.0);
	vs_out.normal = normalize(aNormMat * aNorm);
}