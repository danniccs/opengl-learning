#version 430 core
layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 aNorm;
layout(location = 2) in vec2 atexCoords;

out VS_OUT {
	vec3 normal;
} vs_out;

uniform mat4 model;
uniform mat4 view;
uniform mat3 normMat;

void main() {
	gl_Position = view * model * vec4(aPos, 1.0);
	vs_out.normal = normalize(normMat * aNorm);
}